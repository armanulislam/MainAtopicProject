<?php

include_once ('../../../vendor/autoload.php');
use App\BITM\SEIP137028\City\City;

$city = new \App\BITM\SEIP137028\City\City();

$city->prepareVariableValue($_GET);

$city->recover();