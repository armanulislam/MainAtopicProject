<?php
session_start();
include_once('../../../vendor/autoload.php');
use App\BITM\SEIP137028\Message;
use App\BITM\SEIP137028\Utility;
use App\BITM\SEIP137028\City\City;

$city= new City();
$availablecity=$city->getAllcity();
$comma_separated= '"'.implode('","',$availablecity).'"';
$description_city=$city->getalldescription();
$commaSeparated= '"'.implode('","',$description_city).'"';



if(array_key_exists('itemPerPage',$_SESSION)) {
    if (array_key_exists('itemPerPage', $_GET)) {
        $_SESSION['itemPerPage'] = $_GET['itemPerPage'];
    }
}
else{
    $_SESSION['itemPerPage']=5;
}

//Utility::dd($_SESSION['itemPerPage']);

$itemPerPage=$_SESSION['itemPerPage'];
$totalItem=$city->count();



$totalPage=ceil($totalItem/$itemPerPage);
//Utility::dd($itemPerPage);
$pagination="";


if(array_key_exists('pageNumber',$_GET)){
    $pageNumber=$_GET['pageNumber'];
}else{
    $pageNumber=1;
}
for($i=1;$i<=$totalPage;$i++){
    $class=($pageNumber==$i)?"active":"";
    $pagination.="<li class='$class'><a href='index.php?pageNumber=$i'>$i</a></li>";
}

$pageStartFrom=$itemPerPage*($pageNumber-1);
if(strtoupper($_SERVER['REQUEST_METHOD']=='GET')) {
    $allCity = $city->paginator($pageStartFrom, $itemPerPage);
}
if(strtoupper($_SERVER['REQUEST_METHOD']=='POST')) {
    $allCity = $city->prepareVariableValue($_POST)->index();
}
if((strtoupper($_SERVER['REQUEST_METHOD']=='GET'))&& isset($_GET['search'])) {
    $allCity = $city->prepareVariableValue($_GET)->index();
}



?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
</head>
<body>

<div class="container">
    <h2>City List</h2>

    <a href="../../../Home.php" class="btn btn-success" role="button">Home</a>
    <a href="create.php" class="btn btn-warning" role="button">Create again</a>
    <a href="trashed.php" class="btn btn-warning" role="button">View Trashed list</a>
    <a href="pdf.php" class="btn btn-warning" role="button">Download as PDF</a>
    <a href="xl.php" class="btn btn-warning" role="button">Download as Excel</a>
    <a href="mail.php" class="btn btn-warning" role="button">Email to friend</a><div id='message'>
        <?php
        if (array_key_exists('message', $_SESSION) && (!empty($_SESSION['message'])))
            echo Message::examineMessage();
        ?>
    </div>
    <form role="form">
        <div class="form-group">
            <br><label for="sel1">Select homw many items you want to show (select one):</label>
            <!-- <select class="form-control"  name="itemPerPage" >
                 <option>5</option>
                 <option selected>10</option>
                 <option >15</option>
                 <option>20</option>
                 <option>25</option>
             </select>-->
            <input type="number" class="form-control" name="itemPerPage" placeholder="number must in positive"><br>

            <button type="submit">Go!</button>
        </div>
    </form>
    <table class="table table-hover">
        <thead>
        <tr>
            <th>SL</th>
            <th>ID</th>
            <th>name</th>
            <th>email</th>
            <th>description</th>

            <th>City</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>

        <!--        Here getAllBookData is a object -->
        <?php
        $serialNumber = 1;
        foreach ($allCity as $city) {
            ?>
            <tr>
                <td><?php echo $serialNumber++  + $pageStartFrom ?></td>
                <td><?php echo $city->id ?></td>
                <td><?php echo $city->name ?></td>
                <td><?php echo $city->email ?></td>
                <td><?php echo $city->description ?></td>
                <td><?php echo $city->city ?></td>
                <td>
                    <a href="view.php?id=<?php echo $city->id ?>" class="btn btn-info" role="button">View</a>
                    <a href="edit.php?id=<?php echo $city->id ?>" class="btn btn-primary" role="button">Edit</a>
                    <a href="delete.php?id=<?php echo $city->id ?>" class="btn btn-danger" role="button"
                    onclick="return confirmDelete()"

                    ">Delete</a>
                    <a href="trash.php?id=<?php echo $city->id ?>" class="btn btn-warning" role="button">Trash</a>
                    <a href="mail.php?id=<?= $bookItem['id'] ?>&email=<?= $bookItem['email'] ?>" class="btn btn-primary">Email</a>

                </td>
            </tr>

            <?php
        }
        ?>
        </tbody>
    </table>
    <ul class="pagination">
        <li
            <?php
            if ($pageNumber == 1) {

                ?>
                class="hidden"

                <?php
            }
            ?>
        ><a href="index.php?pageNumber= <?php echo($pageNumber - 1) ?>>">Prev</a></li>
        <?php echo $pagination ?>
        <li
            <?php
            if ($pageNumber == $totalPage) {

                ?>
                class="hidden"

                <?php
            }
            ?>
        ><a href="index.php?pageNumber= <?php echo($pageNumber + 1) ?>>">Next</a></li>
    </ul>
</div>

<script>

    $('#message').show().delay(2000).fadeOut();

    function confirmDelete() {
        var x = confirm("Do you want to delete?");

        if(x)
            return true;
        else return false;
    }


</script>

</body>
</html>

