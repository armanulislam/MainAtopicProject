<!DOCTYPE html>
<html lang="en">
<head>
    <title>email subscription</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <br><a href="index.php" class="btn btn-info" role="button">back to index page</a>
    <h2>Enter your email</h2>
    <form class="form-inline" role="form" method="post"  action="store.php">
        <div class="form-group">
            <label class="sr-only" for="email">Email:</label>
            <input type="email" name="email" class="form-control"  placeholder="Enter email">
        </div>
        <button type="submit" class="btn btn-success">Submit</button>
    </form>
</div>

</body>
</html>

