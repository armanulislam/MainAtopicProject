<?php
include_once('../../../vendor/autoload.php');

use App\BITM\SEIP137028\Email\Email;
use App\BITM\SEIP137028\Message;
use App\BITM\SEIP137028\Utility;

$update=new Email();
$update->prepare($_POST);
$update->edit();