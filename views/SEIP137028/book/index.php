<?php
session_start();
include_once('../../../vendor/autoload.php');
use App\BITM\SEIP137028\Message;
use App\BITM\SEIP137028\Utility;
use App\BITM\SEIP137028\Book\Book;

$book= new Book();
$availableTitle=$book->getAllTitle();
$comma_separated= '"'.implode('","',$availableTitle).'"';
$description_book=$book->getalldescription();
$commaSeparated= '"'.implode('","',$description_book).'"';

//Utility::d($allBook);

if(array_key_exists('itemPerPage',$_SESSION)) {
    if (array_key_exists('itemPerPage', $_GET)) {
        $_SESSION['itemPerPage'] = $_GET['itemPerPage'];
    }
}
else{
    $_SESSION['itemPerPage']=5;
}

//Utility::dd($_SESSION['itemPerPage']);

$itemPerPage=$_SESSION['itemPerPage'];
$totalItem=$book->count();



$totalPage=ceil($totalItem/$itemPerPage);
//Utility::dd($itemPerPage);
$pagination="";


if(array_key_exists('pageNumber',$_GET)){
    $pageNumber=$_GET['pageNumber'];
}else{
    $pageNumber=1;
}
for($i=1;$i<=$totalPage;$i++){
    $class=($pageNumber==$i)?"active":"";
    $pagination.="<li class='$class'><a href='index.php?pageNumber=$i'>$i</a></li>";
}

$pageStartFrom=$itemPerPage*($pageNumber-1);
if(strtoupper($_SERVER['REQUEST_METHOD']=='GET')) {
    $allBook = $book->paginator($pageStartFrom, $itemPerPage);
}
if(strtoupper($_SERVER['REQUEST_METHOD']=='POST')) {
    $allBook = $book->prepare($_POST)->index();
}
if((strtoupper($_SERVER['REQUEST_METHOD']=='GET'))&& isset($_GET['search'])) {
    $allBook = $book->prepare($_GET)->index();
}



?>

<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
</head>
<body class="bg-info">

<div class="container">
    <h2>All Book List</h2>
    <a href="../../../Home.php" class="btn btn-success" role="button">Home</a>
    <a href="create.php" class="btn btn-warning" role="button">Create again</a>  <a href="trashed.php" class="btn btn-warning" role="button">View Trashed list</a>
    <a href="pdf.php" class="btn btn-warning" role="button">Download as PDF</a>
    <a href="xl.php" class="btn btn-warning" role="button">Download as Excel</a>
    <a href="mail.php" class="btn btn-warning" role="button">Email to friend</a>
    <div id="message">

        <?php if((array_key_exists('message',$_SESSION)&& (!empty($_SESSION['message'])))) {
            echo Message::examineMessage();
        }
        ?>
    </div>
    <form role="form">
        <div class="form-group">
            <br><label for="sel1">Select homw many items you want to show (select one):</label>

            <input type="number" class="form-control" name="itemPerPage" placeholder="number must in positive"><br>

            <button type="submit">Go!</button>

        </div>
    </form>
    <form action="index.php" method="post">
        <label>Filter by Title</label>
        <input type="text" name="filterByTitle" value="" id="title">
        <label>Filter by Description</label>
        <input type="text" name="filterByDescription" value="" id="description">
        <button type="submit">Submit</button>
    </form>
    <form action="index.php" method="get">
        <label>Search</label>
        <input type="text" name="search" value="">
        <button type="submit">Search</button>
    </form>
    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th>#</th>
                <th>ID</th>
                <th>Book title</th>
                <th>email</th>
                <th>Description</th>
                <th>Action</th>

            </tr>
            </thead>
            <tbody>
            <tr>
                <?php
                $sl=0;
                foreach($allBook as $book){
                $sl++; ?>
                <td><?php echo $sl+$pageStartFrom?></td>
                <td><?php echo $book-> id?></td>
                <td><?php echo $book->title?></td>
                <td><?php echo $book->email?></td>
                <td><?php echo $book->description?></td>
                <td><a href="view.php?id=<?php echo $book-> id ?>" class="btn btn-primary" role="button">View</a>
                    <a href="edit.php?id=<?php echo $book-> id ?>"  class="btn btn-info" role="button">Edit</a>
                    <a href="delete.php?id=<?php echo $book->id?>" class="btn btn-danger" role="button" id="delete"  Onclick="return ConfirmDelete()">Delete</a>
                    <a href="trash.php?id=<?php echo $book->id ?>"  class="btn btn-info" role="button">Trash</a>
                    <a href="mail.php?id=<?= $bookItem['id'] ?>&email=<?= $bookItem['email'] ?>" class="btn btn-primary">Email</a>
                </td>

            </tr>
            <?php }?>


            </tbody>
        </table>
    </div>
    <?php if(strtoupper($_SERVER['REQUEST_METHOD']=='GET')) { ?>
        <ul class="pagination">
            <li><a href="#">Prev</a></li>
            <?php echo $pagination?>
            <li><a href="#">Next</a></li>
        </ul>
    <?php } ?>
</div>
<script>
    $('#message').show().delay(2000).fadeOut();


    //        $(document).ready(function(){
    //            $("#delete").click(function(){
    //                if (!confirm("Do you want to delete")){
    //                    return false;
    //                }
    //            });
    //        });
    function ConfirmDelete()
    {
        var x = confirm("Are you sure you want to delete?");
        if (x)
            return true;
        else
            return false;
    }

</script>
<script>
    $( function() {
        var availableTags = [
            <?php echo $comma_separated?>
        ];
        $( "#title" ).autocomplete({
            source: availableTags
        });
    } );
</script>
<script>
    $( function() {
        var availableTags = [
            <?php echo $commaSeparated?>
        ];
        $( "#description" ).autocomplete({
            source: availableTags
        });
    } );
</script>

</body>
</html>
