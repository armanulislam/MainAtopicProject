<!DOCTYPE html>
<html lang="en">
<head>
    <title>Book titles</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <br><a href="index.php" class="btn btn-success" role="button">Home</a>
    <h2>Create Book Title</h2>
    <form role="form" method="post" action="store.php">
        <div class="form-group">
            <label>Enter Book Title:</label>
            <input type="text" name="title" class="form-control" id="title" placeholder="Enter Book Title">
        </div>
        <button type="reset" class="btn btn-warning">reset</button>
        <button type="submit" class="btn btn-success">Submit</button>

    </form>
</div>


</body>
</html>
