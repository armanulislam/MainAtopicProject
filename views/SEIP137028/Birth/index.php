<?php
session_start();
include_once("../../../vendor/autoload.php");
use App\BITM\SEIP137028\Utility;
use App\BITM\SEIP137028\Message;


$birth= new \App\BITM\SEIP137028\Birth\Birth();

//Utility::d($allBook);

if(array_key_exists('itemPerPage',$_SESSION)) {
    if (array_key_exists('itemPerPage', $_GET)) {
        $_SESSION['itemPerPage'] = $_GET['itemPerPage'];
    }
}
else{
    $_SESSION['itemPerPage']=5;
}

//Utility::dd($_SESSION['itemPerPage']);

$itemPerPage=$_SESSION['itemPerPage'];
$totalItem=$birth->count();



$totalPage=ceil($totalItem/$itemPerPage);
//Utility::dd($itemPerPage);
$pagination="";


if(array_key_exists('pageNumber',$_GET)){
    $pageNumber=$_GET['pageNumber'];
}else{
    $pageNumber=1;
}
for($i=1;$i<=$totalPage;$i++){
    $class=($pageNumber==$i)?"active":"";
    $pagination.="<li class='$class'><a href='index.php?pageNumber=$i'>$i</a></li>";
}

$pageStartFrom=$itemPerPage*($pageNumber-1);
$getAllBirthData=$birth->paginator($pageStartFrom,$itemPerPage);

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Birthday</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body class="bg-info">

<div class="container">
    <center><h2>All user birthday list</h2></center>
    <a href="../../../Home.php" class="btn btn-success" role="button">Home</a>
    <a href="create.php" class="btn btn-info" role="button">Create again</a>
    <a href="trashed.php" class="btn btn-info" role="button">View Trashed list</a>
    <a href="pdf.php" class="btn btn-info" role="button">Download as PDF</a>
    <a href="xl.php" class="btn btn-info" role="button">Download as XL</a>
    <a href="mail.php" class="btn btn-info" role="button">Email to friend</a><div id = 'message'>
        <?php
        if (array_key_exists('message', $_SESSION) && (!empty($_SESSION['message'])))
            echo Message::examineMessage();
        ?>
    </div>

        <div class="form-group">
            <br><label for="sel1">Select homw many items you want to show (select one):</label>
            <input type="number" class="form-control" name="itemPerPage" placeholder="number must in positive"><br>

        </div>
    <table class="table table-hover">
        <thead>
        <tr>
            <th>SL</th>
            <th>ID</th>
            <th>name</th>
            <th>Date</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>

        <!--        Here getAllBookData is a object -->
        <?php
        $serialNumber = 1;
        foreach ($getAllBirthData as $birth) {
            $date = "";
            ?>
            <tr>
                <td><?php echo $serialNumber++  + $pageStartFrom ?></td>
                <td><?php echo $birth['id'] ?></td>
                <td><?php echo $birth['name'] ?></td>

                <?php
                $contain = explode("-", $birth['date']);

                $date = $contain[2]. "-" . $contain[1] . "-" . $contain[0];

                ?>

                <td><?php echo $date ?></td>
                <td>
                    <a href="view.php?id=<?php echo $birth['id']?>" class="btn btn-info" role="button">View</a>
                    <a href="edit.php?id=<?php echo $birth['id']?>" class="btn btn-primary" role="button">Edit</a>
                    <a href="delete.php?id=<?php echo $birth['id']?>" class="btn btn-danger" role="button">Delete</a>
                    <a href="trash.php?id=<?php echo $birth['id']?>" class="btn btn-warning" role="button">Trash</a>
                </td>
            </tr>

            <?php
        }
        ?>
        </tbody>
    </table>
    <ul class="pagination">
        <li
            <?php
            if ($pageNumber == 1) {

                ?>
                class="hidden"

                <?php
            }
            ?>
        ><a href="index.php?pageNumber= <?php echo($pageNumber - 1) ?>>">Prev</a></li>
        <?php echo $pagination ?>
        <li
            <?php
            if ($pageNumber == $totalPage) {

                ?>
                class="hidden"

                <?php
            }
            ?>
        ><a href="index.php?pageNumber= <?php echo($pageNumber + 1) ?>>">Next</a></li>
    </ul>
</div>

<script>

    $('#message').show().delay(2000).fadeOut();
</script>

</body>
</html>

