<?php
session_start();
include_once("../../../vendor/autoload.php");
use App\BITM\SEIP137028\Hobby\Hobby;
use App\BITM\SEIP137028\Utility;
use App\BITM\SEIP137028\Message;

////Creating object
//$hobby = new Hobby();
////Getting DB data as object form
//$getAllHobbyData = $hobby->index();
//// Checking DB data
////Utility::dd($getAllHobbyData);

$hobby= new Hobby();

//Utility::d($allBook);

if(array_key_exists('itemPerPage',$_SESSION)) {
    if (array_key_exists('itemPerPage', $_GET)) {
        $_SESSION['itemPerPage'] = $_GET['itemPerPage'];
    }
}
else{
    $_SESSION['itemPerPage']=5;
}

//Utility::dd($_SESSION['itemPerPage']);

$itemPerPage=$_SESSION['itemPerPage'];
$totalItem=$hobby->count();



$totalPage=ceil($totalItem/$itemPerPage);
//Utility::dd($itemPerPage);
$pagination="";


if(array_key_exists('pageNumber',$_GET)){
    $pageNumber=$_GET['pageNumber'];
}else{
    $pageNumber=1;
}
for($i=1;$i<=$totalPage;$i++){
    $class=($pageNumber==$i)?"active":"";
    $pagination.="<li class='$class'><a href='index.php?pageNumber=$i'>$i</a></li>";
}

$pageStartFrom=$itemPerPage*($pageNumber-1);
$getAllHobbyData=$hobby->paginator($pageStartFrom,$itemPerPage);

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body class="bg-info">

<div class="container">
    <h2>Hobby List</h2>
    <a href="../../../Home.php" class="btn btn-success" role="button">Home</a>
    <a href="create.php" class="btn btn-info" role="button">Create Again</a>
    <a href="trashed.php" class="btn btn-info" role="button">Go to trashed</a>
    <div id = 'message'>
        <?php
        if (array_key_exists('message', $_SESSION) && (!empty($_SESSION['message'])))
            echo Message::examineMessage();
        ?>
    </div>
    <form role="form">
        <div class="form-group">
            <br><label for="sel1">Select homw many items you want to show (select one):</label>
            <!-- <select class="form-control"  name="itemPerPage" >
                 <option>5</option>
                 <option selected>10</option>
                 <option >15</option>
                 <option>20</option>
                 <option>25</option>
             </select>-->
            <input type="number" class="form-control" name="itemPerPage" placeholder="number must in positive"><br>

            <button type="submit">Go!</button>
        </div>
    </form>

    <table class="table table-hover">
        <thead>
        <tr>
            <th>SL</th>
            <th>ID</th>
            <th>hobby</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>

        <!--        Here getAllBookData is a object -->
        <?php
        $serialNumber = 1;
        foreach ($getAllHobbyData as $hobby) {
            ?>
            <tr>
                <td><?php echo $serialNumber++  + $pageStartFrom ?></td>
                <td><?php echo $hobby['id'] ?></td>
                <td><?php echo $hobby['hobby'] ?></td>
                <td>
                    <a href="view.php?id=<?php echo $hobby['id']?>" class="btn btn-info" role="button">View</a>
                    <a href="edit.php?id=<?php echo $hobby['id']?>" class="btn btn-primary" role="button">Edit</a>
                    <a href="delete.php?id=<?php echo $hobby['id']?>" class="btn btn-danger" role="button">Delete</a>
                    <a href="trash.php?id=<?php echo $hobby['id']?>" class="btn btn-warning" role="button">Trash</a>
                </td>
            </tr>

            <?php
        }
        ?>
        </tbody>
    </table>
    <ul class="pagination">
        <li
            <?php
            if ($pageNumber == 1) {

                ?>
                class="hidden"

                <?php
            }
            ?>
        ><a href="index.php?pageNumber= <?php echo($pageNumber - 1) ?>>">Prev</a></li>
        <?php echo $pagination ?>
        <li
            <?php
            if ($pageNumber == $totalPage) {

                ?>
                class="hidden"

                <?php
            }
            ?>
        ><a href="index.php?pageNumber= <?php echo($pageNumber + 1) ?>>">Next</a></li>
    </ul>
</div>

<script>

    $('#message').show().delay(2000).fadeOut();
</script>

</body>
</html>

