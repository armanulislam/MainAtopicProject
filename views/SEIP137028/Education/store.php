<?php

include_once ('../../../vendor/autoload.php');
use App\BITM\SEIP137028\Education\Education;
use App\BITM\SEIP137028\Utility;


$education = new Education();
$education->prepareVariableValue($_POST);
$education->store();
?>