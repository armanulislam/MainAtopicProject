<?php
session_start();
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP137028\ProfilePicture\ImageUploader;
use App\Bitm\SEIP137028\Utility;
use App\Bitm\SEIP137028\Message;

$profile_picture = new ImageUploader();
$allinfo = $profile_picture->trashed();
//Utility::d($allBook);


?>

<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

</head>
<body>

<div class="container">
    <h2>All Info List</h2>
    
    <form action="multiple.php" method="post">
        <a href="index.php" class="btn btn-info" role="button">Home</a>
        <button name = "recoverAll" class="btn btn-warning" role="button">Recover All</button>
        <button name = "deleteAll" class="btn btn-danger" role="button">Delete All</button>

        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Image</th>
                    <th>Action</th>

                </tr>
                </thead>
                <tbody>
                <tr>
                    <?php
                    $sl = 0;
                    foreach ($allinfo as $info){
                    $sl++; ?>
                    <td><input type="checkbox" name="id[]" value="<?php echo $info->id ?>"></td>

                    <td><?php echo $info->id ?></td>
                    <td><?php echo $info->name ?></td>
                    <td><img src="../../../Resources/Images/<?php echo $info->images ?>" alt="image" height="100px"
                             width="100px" class="img-responsive"></td>
                    <td>
                        <a href="recover.php?id=<?php echo $info->id ?>" class="btn btn-info" role="button">Recover</a>
                        <a href="delete.php?id=<?php echo $info->id ?>" class="btn btn-danger" role="button" id="delete"
                        ">Delete</a>

                    </td>

                </tr>
                <?php } ?>


                </tbody>
            </table>
        </div>
    </form>
</div>

</body>
</html>
