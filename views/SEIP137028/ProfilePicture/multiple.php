<?php

include "../../../vendor/autoload.php";
use App\BITM\SEIP137028\ProfilePicture\ImageUploader;
$propic = new ImageUploader();
if(array_key_exists('recoverAll', $_POST)) {
    $propic->recoverSelected($_POST['id']);
}


if(array_key_exists('deleteAll', $_POST)) {
    $propic->deleteSelected($_POST['id']);
}