<?php
include_once ('../../../vendor/autoload.php');
use App\Bitm\SEIP137028\ProfilePicture\ImageUploader;


$profile_picture= new ImageUploader();
$single_info=$profile_picture->prepare($_GET)->view();
unlink($_SERVER['DOCUMENT_ROOT'].'/day13/Resources/Images/'.$single_info->images);
$profile_picture->prepare($_GET)->delete();