-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 21, 2016 at 12:38 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `atomicprojectb22`
--

-- --------------------------------------------------------

--
-- Table structure for table `book`
--

CREATE TABLE IF NOT EXISTS `book` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `deleted_at` int(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `book`
--

INSERT INTO `book` (`id`, `title`, `email`, `description`, `deleted_at`) VALUES
(18, '6669', 'dfkdfdjfo@dfdkfdlfj', '420', NULL),
(22, 'hhhhh', '', '<p>kjlkjkjl</p>', NULL),
(23, 'jjhkjhoiuuih', '', '<p>jkhkjjkh</p>', NULL),
(24, 'tilte', '', '<p>book</p>', NULL),
(25, 'salam', '', '<p>rafiq</p>', NULL),
(26, 'welcome to site', '', '<p>thisi is another title<strong> hi dear<em> :P</em></strong></p>', NULL),
(27, 'pppppppppp', 'aaa@aaaa', 'lllll', NULL),
(28, 'mkjjk', 'jjj@hjhj', '<p>asdf</p>', NULL),
(29, 'arman', 'arman.bspi88@gmail.com', '<p>hi</p>', NULL),
(30, 'arman', 'arman.bspi88@gmail.com', '<p>this is another title</p>', NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
