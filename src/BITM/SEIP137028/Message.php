<?php
namespace App\BITM\SEIP137028;

if(!isset($_SESSION['message']))
    session_start();

class Message
{
    public static function examineMessage($message = null)
    {
        // We are checking is message null?
        // if yes
        // we invoke to get the message to show
        if (is_null($message)) {
            return self::getMessage();
        } else {

            // if it is not null
            // so we invoke to set the message
            // as it is not null
            // we have to contain the message
            // to show it later
            self::setMessage($message);
        }
    }



    public static function setMessage($message)
    {
        $_SESSION['message'] = $message;
    }

    public static function getMessage()
    {
        // keep the message to send
        $gotTheMessage = $_SESSION['message'];
        // Assigning null to SESSION
        $_SESSION['message'] = "";
        return $gotTheMessage;
    }
}